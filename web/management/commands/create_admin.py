from pathlib import Path

from django.contrib.auth.models import User
from django.core.management import BaseCommand

log = Path(__file__).stem


class Command(BaseCommand):
    help = "Create super user from command line"

    def add_arguments(self, parser):
        parser.add_argument("--name", type=str, default="admin", help="user name")
        parser.add_argument(
            "--email", type=str, default="admin@mail.ru", help="user email"
        )
        parser.add_argument("--pwd", type=str, default="admin", help="user password")

    def handle(self, *args, **options):
        name = options["name"]
        email = options["email"]
        pwd = options["pwd"]

        self.stdout.write(f"{log} try to create admin with username '{name}'")
        admin = User.objects.filter(username=name).first()
        if admin:
            admin.delete()
        try:
            User.objects.create_superuser(username=name, email=email, password=pwd)
        except Exception as e:
            self.stdout.write(f"{log} failed due to {repr(e)}")
        else:
            self.stdout.write(f"{log} username '{name}' successfully created")
