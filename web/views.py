from django.conf import settings
from django.http import HttpRequest, HttpResponse, JsonResponse

from web.models import UrlResponse, send_404


def web_request(request: HttpRequest, *args, **kwargs):
    url = request.path
    method = request.method
    try:
        obj = UrlResponse.objects.get(method=method, url=url)
        return JsonResponse(
            obj.response,
            status=obj.status,
            content_type="application/json",
        )

    except UrlResponse.DoesNotExist:
        if send_404():
            return JsonResponse(
                {"error": f"unknown: {method} {url}"},
                status=404,
                content_type="application/json",
            )
        else:
            return HttpResponse(status=200)


def root_view(request: HttpRequest, *args, **kwargs):
    return JsonResponse(
        {
            "service": "Mock Web Service",
            "version": settings.VERSION,
            "send 404": send_404(),
        },
        content_type="application/json",
    )
