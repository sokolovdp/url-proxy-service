from django.contrib import admin

from web.models import Parameters, UrlResponse


@admin.register(UrlResponse)
class UrlResponseAdmin(admin.ModelAdmin):
    list_display = (
        "method",
        "url",
        "response",
        "status",
    )


@admin.register(Parameters)
class ParametersAdmin(admin.ModelAdmin):
    list_display = ("send_404",)
