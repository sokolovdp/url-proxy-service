from functools import lru_cache

from django.db import models


class UrlResponse(models.Model):
    method = models.CharField(max_length=20)
    url = models.CharField(max_length=200)
    response = models.JSONField()
    status = models.PositiveSmallIntegerField(default=200)

    class Meta:
        verbose_name_plural = "Url Responses"
        ordering = ["url"]
        constraints = [
            models.UniqueConstraint(
                fields=["method", "url"],
                name="unique_method_url",
            )
        ]


class Parameters(models.Model):
    send_404 = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = "Parameters"


@lru_cache
def send_404() -> bool:
    params = Parameters.objects.first()
    if not params:
        params, _ = Parameters.objects.create(send_404=False)

    return params.send_404
