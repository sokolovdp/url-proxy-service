APPS:=./mock_service ./web

.PHONY: pretty lint

pretty:
	black $(APPS)
	isort $(APPS)

lint:
	black $(APPS) --check
	flake8 $(APPS)