FROM python:alpine
COPY . .
WORKDIR .
RUN apk update && apk add curl
RUN pip install -r requirements.txt
